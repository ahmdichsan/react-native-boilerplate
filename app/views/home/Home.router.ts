import Home from './Home';
import { NavigatiounRouter } from '../../interfaces';

const HomeRoutes: NavigatiounRouter = {
  Home: { screen: Home },
};

export default HomeRoutes;
