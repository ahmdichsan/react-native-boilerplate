/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React from 'react';
import { StatusBar, ScrollView, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { SafeAreaView } from 'react-navigation';

/** Region Import Constants */

/** Region Import Interfaces */
import { ExampleProps } from './interfaces';

/** Region Import Redux Action Type and Redux Action */

/** Region Import Utility/Helper Function */
import { convertToSentenceText, greetingSentence } from '../../helpers';

/** Region Import Custom Hook */
import { useAuthSelector } from '../../custom-hook';

/** Region Import Components/Cards */
import { CardBase } from '../../cards';

/** Region Import Assets */

/** Region Import Style */
import HomeStyle from './styles';
import { colors } from '../../styles';

function Home(props: ExampleProps) {
  /** useDispatch declaration */

  /** useState declaration */

  /** props object destruction */

  /** useCustomHook */
  const { authRes } = useAuthSelector();

  /** onHandleFunction declaration */

  /** useEffect declaration */

  /** return element */
  return (
    <LinearGradient
      colors={[colors.blueCompany, colors.white]}
      style={[HomeStyle.linearGradient]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >
      <SafeAreaView style={[HomeStyle.container]} forceInset={{ top: 'always' }}>
        <StatusBar barStyle="light-content" translucent backgroundColor="transparent" />
        <ScrollView style={[HomeStyle.scrollView]}>
          <View style={[HomeStyle.contentWrapper]}>
            <CardBase title={`${greetingSentence()}, ${convertToSentenceText(authRes?.name || '-')}`}>
              <>
                <View>
                  <Text style={[HomeStyle.welcomeText]}>
                    Welcome to React Native Boilerplate
                  </Text>
                </View>
              </>
            </CardBase>
          </View>
        </ScrollView>
      </SafeAreaView>
    </LinearGradient>
  );
};

export default Home;
