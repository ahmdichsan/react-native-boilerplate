import { StyleSheet, StatusBar } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
  },
  contentWrapper: {
    paddingHorizontal: 15,
    paddingVertical: 15,
    flex: 1,
  },
  linearGradient: {
    paddingTop: StatusBar.currentHeight,
    flex: 1,
  },
  welcomeText: {
    textAlign: 'center',
  },
});
