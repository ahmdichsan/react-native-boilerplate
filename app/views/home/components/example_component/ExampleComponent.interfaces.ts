import { NavigationProps, Key } from '../../../../interfaces';

export interface ExampleComponentProps extends NavigationProps, Key { };

export interface ExampleComponentState {
    componentName: string;
};
