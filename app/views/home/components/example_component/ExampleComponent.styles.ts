import { StyleSheet } from 'react-native';
import { colors } from '../../../../styles';

export default StyleSheet.create({
  buttonHome: {
    marginTop: 20,
    width: '100%',
  },
  textStyle: {
    color: colors.black,
  },
});
