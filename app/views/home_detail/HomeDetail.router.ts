import HomeDetail from './HomeDetail';

const HomeDetailRoutes = {
  HomeDetail: { screen: HomeDetail },
};

export default HomeDetailRoutes;
