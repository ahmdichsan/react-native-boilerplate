import { NavigationProps, DefaultSagaParam } from '../../../interfaces';

export interface ExampleProps extends NavigationProps { }

export interface ExampleState {
  isLoading: boolean;
}

export interface ExampleSagaRequest extends DefaultSagaParam {
  data: string;
};
