import { NavigationProps, DefaultSagaParam } from '../../../interfaces';

export interface ProfileProps extends NavigationProps { }

export interface ProfileState {
  isLoading: boolean;
}

export interface ExampleSagaRequest extends DefaultSagaParam {
  data: string;
};
