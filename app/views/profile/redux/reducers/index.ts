/**
 * @description
 * import this file in root > app > config > ReduxReducer.ts
 * 
 * do this if have more than one reducers:
 * 
 * import { combineReducers } from 'redux';
 * import { ReducerOne } from './ReducerOne';
 * import { ReducerTwo } from './ReducerTwo';
 * 
 * export default combineReducers({
 *    reducerOne: ReducerOne,
 *    reducerTwo: ReducerTwo,
 * });
 */

/// ====================================================
/// do this if only has one reducers
export * from './Example.reducers';
