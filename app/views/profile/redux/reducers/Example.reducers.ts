import { EXAMPLE_FETCH, EXAMPLE_FETCH_FAILED, EXAMPLE_FETCH_SUCCESS } from '../actions';
import { DefaultReduxState, DefaultReduxAction } from '../../../../interfaces';
import { defaultState, defaultAction } from '../../../../constants';

export function ReducerExample(state: DefaultReduxState = defaultState, action: DefaultReduxAction = defaultAction) {
  switch (action.type) {
    case EXAMPLE_FETCH:
      return {
        ...state,
        fetch: true,
        data: action.data,
        action: action.type,
      };

    case EXAMPLE_FETCH_FAILED:
      return {
        ...state,
        fetch: false,
        res: action.data,
        action: action.type,
      };

    case EXAMPLE_FETCH_SUCCESS:
      return {
        ...state,
        fetch: false,
        err: action.data,
        action: action.type,
      };

    default:
      return state;
  }
}
