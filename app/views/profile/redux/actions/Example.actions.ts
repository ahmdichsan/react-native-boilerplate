/** put action type and action function here to make it easier to read */

export const EXAMPLE_FETCH = 'EXAMPLE_FETCH';
export const EXAMPLE_FETCH_SUCCESS = 'EXAMPLE_FETCH_SUCCESS';
export const EXAMPLE_FETCH_FAILED = 'EXAMPLE_FETCH_FAILED';
export const fetchExample = (data: object) => ({ type: EXAMPLE_FETCH, data });
export const fetchExampleSuccess = (data: object) => ({ type: EXAMPLE_FETCH_SUCCESS, data });
export const fetchExampleFailed = (data: object) => ({ type: EXAMPLE_FETCH_FAILED, data });
