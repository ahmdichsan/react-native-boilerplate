import {
  // call,
  put,
  takeLatest,
} from 'redux-saga/effects';
import {
  EXAMPLE_FETCH,
  fetchExampleSuccess,
  fetchExampleFailed
} from '../actions';
// import { ApiProfile } from '../../../../constants';
import { ExampleSagaRequest } from '../../interfaces';
// import { HttpService } from '../../../../utilities/StoreApi';

function* workerSagaExample({ data }: ExampleSagaRequest) {
  try {
    /** example API */
    // const endPoint: string = ApiProfile.profile;

    // const response: any = yield call(HttpService.post, url, data);

    /** you can do something here first before update to the redux store */
    yield put(fetchExampleSuccess({}));
  } catch (e) {
    yield put(fetchExampleFailed(e));
  }
}

export const watcherExample = [
  takeLatest(EXAMPLE_FETCH, workerSagaExample),
];
