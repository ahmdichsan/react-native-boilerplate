/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React from 'react';
import { StatusBar, ScrollView, View, Alert } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux';

/** Region Import Constants */

/** Region Import Interfaces */
import { ProfileProps } from './interfaces';

/** Region Import Redux Action Type and Redux Action */
import { userLogout } from '../../redux/Global.actions';

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */
import { useAuthSelector, useToDoSelector } from '../../custom-hook';

/** Region Import Components/Cards */
import { CardBase } from '../../cards';
import { TextBase, ButtonDefault } from '../../components';

/** Region Import Assets */

/** Region Import Style */
import ProfileStyle from './styles';
import { colors } from '../../styles';

function Profile(props: ProfileProps) {
  /** useDispatch declaration */
  const dispatch = useDispatch();

  /** useState declaration */

  /** props object destruction */

  /** useCustomHook */
  const { authRes } = useAuthSelector();
  const { resToDo } = useToDoSelector();

  /** onHandleFunction declaration */
  function doSignOut() {
    dispatch(userLogout());
    props.navigation.navigate('Login');
  };

  function confirmSignOut() {
    if (!resToDo) {
      doSignOut();
      return;
    }

    if (resToDo.find(item => !item.isSynced)) {
      Alert.alert('Attention!', 'You have not synced to do. This action will delete them. Are you sure?', [
        {
          onPress: doSignOut,
          style: 'default',
          text: 'Yes',
        },
        {
          text: 'No',
          style: 'cancel',
        },
      ], {
        cancelable: true,
      });
      return;
    }

    doSignOut();
  };

  /** useEffect declaration */

  /** return element */
  return (
    <LinearGradient
      colors={[colors.blueCompany, colors.white]}
      style={[ProfileStyle.linearGradient]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >
      <SafeAreaView style={[ProfileStyle.container]} forceInset={{ top: 'always' }}>
        <StatusBar barStyle="light-content" translucent backgroundColor="transparent" />
        <ScrollView style={[ProfileStyle.scrollView]}>
          <View style={[ProfileStyle.contentWrapper]}>
            <CardBase>
              <>
                <View>
                  <TextBase>
                    {`Name: ${authRes?.name || '-'}`}
                  </TextBase>
                </View>
                <View>
                  <TextBase>
                    {`UserName: ${authRes?.userName || '-'}`}
                  </TextBase>
                </View>
                <View style={[{ marginTop: 10 }]}>
                  <ButtonDefault
                    onPress={confirmSignOut}
                    text="Sign Out"
                  />
                </View>
              </>
            </CardBase>
          </View>
        </ScrollView>
      </SafeAreaView>
    </LinearGradient>
  );
};

export default Profile;
