import Profile from './Profile';

const ProfileRoutes = {
  Profile: { screen: Profile },
};

export default ProfileRoutes;
