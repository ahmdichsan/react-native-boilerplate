import { NavigationProps, DefaultSagaParam } from '../../../interfaces';

export interface ProductProps extends NavigationProps { }

export interface ProductState {
  isLoading: boolean;
}

export interface ExampleSagaRequest extends DefaultSagaParam {
  data: string;
};
