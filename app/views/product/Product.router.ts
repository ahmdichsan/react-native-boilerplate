import Product from './Product';

const ProductRoutes = {
  Product: { screen: Product },
};

export default ProductRoutes;
