/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { withNavigation } from 'react-navigation';

/** Region Import Constants */
import { initialState } from './ExampleComponent.constants';

/** Region Import Interfaces */
import { ExampleComponentProps, ExampleComponentState } from './ExampleComponent.interfaces';

/** Region Import Redux Action Type and Redux Action */

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */

/** Region Import Components/Cards */

/** Region Import Assets */

/** Region Import Style */
import ExampleComponentStyle from './ExampleComponent.styles';

function ExampleComponent(props: ExampleComponentProps) {
  /** useDispatch declaration */

  /** useState declaration */
  const [state, setState] = useState<ExampleComponentState>(initialState);

  /** props object destruction */

  /** useCustomHook */

  /** onHandleFunction declaration */

  /** useEffect declaration */
  useEffect(() => {
    setState({
      ...state,
      componentName: 'Product',
    });
  }, []);

  /** return element */
  return (
    <View>
      <Text style={[ExampleComponentStyle.textStyle]}>
        Component {state.componentName}
      </Text>
    </View>
  );
};

/**
 * using withNavigation to make this child component could access navigation props
 * remove it if not needed
*/
export default withNavigation(ExampleComponent);
