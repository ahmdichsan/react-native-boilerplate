/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React from 'react';
import { ScrollView, StatusBar, View } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';

/** Region Import Constants */

/** Region Import Interfaces */
import { ProductProps } from './interfaces';

/** Region Import Redux Action Type and Redux Action */

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */

/** Region Import Components/Cards */
import { ExampleComponent } from './components';

/** Region Import Assets */

/** Region Import Style */
import ProductStyle from './styles';
import { colors } from '../../styles';

function Product(props: ProductProps) {
  /** useDispatch declaration */

  /** useState declaration */

  /** props object destruction */

  /** useCustomHook */

  /** onHandleFunction declaration */

  /** useEffect declaration */

  /** return element */
  return (
    <LinearGradient
      colors={[colors.blueCompany, colors.white]}
      style={[ProductStyle.linearGradient]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >
      <SafeAreaView style={[ProductStyle.container]} forceInset={{ top: 'always' }}>
        <StatusBar barStyle="light-content" translucent backgroundColor="transparent" />
        <ScrollView style={[ProductStyle.scrollView]}>
          <View style={[ProductStyle.contentWrapper]}>
            <ExampleComponent />
          </View>
        </ScrollView>
      </SafeAreaView>
    </LinearGradient>
  );
};

export default Product;
