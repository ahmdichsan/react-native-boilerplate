import { NavigationProps, Key } from '../../../../interfaces';

export interface ExampleCardProps extends NavigationProps, Key { };

export interface ExampleCardState {
    componentName: string;
};
