/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { withNavigation } from 'react-navigation';

/** Region Import Constants */
import { initialState } from './ExampleCard.constants';

/** Region Import Interfaces */
import { ExampleCardProps, ExampleCardState } from './ExampleCard.interfaces';

/** Region Import Redux Action Type and Redux Action */

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */

/** Region Import Components/Cards */
import { ButtonDanger } from '../../../../components';

/** Region Import Assets */

/** Region Import Style */
import ExampleCardStyle from './ExampleCard.styles';

function ExampleCard(props: ExampleCardProps) {
  /** useDispatch declaration */

  /** useState declaration */
  const [state, setState] = useState<ExampleCardState>(initialState);

  /** props object destruction */

  /** useCustomHook */

  /** onHandleFunction declaration */
  function goToHome() {
    props.navigation.navigate('Home');
  };

  /** useEffect declaration */
  useEffect(() => {
    setState({
      ...state,
      componentName: 'Example',
    });
  }, []);

  /** return element */
  return (
    <View>
      <Text style={[ExampleCardStyle.textStyle]}>
        Component {state.componentName}
      </Text>
      <ButtonDanger
        text="Home"
        onPress={goToHome}
        containerStyle={[ExampleCardStyle.buttonHome]}
      />
    </View>
  );
};

/**
 * using withNavigation to make this child component could access navigation props
 * remove it if not needed
*/
export default withNavigation(ExampleCard);
