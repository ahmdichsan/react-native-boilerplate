import { DefaultSagaParam, NavigationProps, LoginPayload } from '../../../interfaces';

export interface LoginProps extends NavigationProps { };

export interface LoginState { };

export interface LoginSagaRequest extends DefaultSagaParam {
  data: LoginPayload;
};
