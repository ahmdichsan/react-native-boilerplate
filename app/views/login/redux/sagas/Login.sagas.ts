import {
  call,
  put,
  takeLatest,
} from 'redux-saga/effects';
import {
  AUTH_FETCH,
  fetchAuthSuccess,
  fetchAuthFailed
} from '../actions';
import { Auth } from '../../../../interfaces';
import { LoginSagaRequest } from '../../interfaces';
import { apiIdentity } from '../../../../constants';
import { HttpService } from '../../../../helpers';

function* workerSagaAuth({ data }: LoginSagaRequest) {
  try {
    const body = {
      userName: data.userName,
    };

    const headers = {
      information: data.password,
    };

    const endPoint = apiIdentity.login;

    const response = yield call(HttpService.post, endPoint, body, headers);

    const parseResponse: Auth = {
      id: response.data.id,
      jwt: {
        expired: response.headers.exp,
        token: response.headers.content,
      },
      name: response.data.name,
      userName: response.data.userName,
    };

    yield put(fetchAuthSuccess(parseResponse));
  } catch (error) {
    yield put(fetchAuthFailed(error.message));
  }
}

export const watcherAuth = [
  takeLatest(AUTH_FETCH, workerSagaAuth),
];
