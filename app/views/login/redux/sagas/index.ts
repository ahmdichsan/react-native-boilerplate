/**
 * @description
 * import this file in root > app > config > ReduxSaga.ts
 * 
 * do this if have more than one sagas:
 * 
 * import { watcherExampleOne } from './ExampleOne.sagas';
 * import { watcherExampleTwo } from './ExampleTwo.sagas';
 * 
 * export const watcherExampleAll = [
 *    ...watcherExampleOne,
 *    ...watcherExampleTwo,
 * ];
 */

/// ====================================================
/// do this if only has one reducers
export * from './Login.sagas';
