import { AUTH_FETCH, AUTH_FETCH_FAILED, AUTH_FETCH_SUCCESS } from '../actions';
import { DefaultReduxAction, LoginPayload, Auth } from '../../../../interfaces';
import { defaultState, defaultAction } from '../../../../constants';
import { AuthSelector } from '../../../../custom-hook';

export function ReducerAuth(state: AuthSelector = defaultState, action: DefaultReduxAction<LoginPayload, Auth> = defaultAction): AuthSelector {
  switch (action.type) {
    case AUTH_FETCH:
      return {
        ...state,
        fetch: true,
        data: action.data || null,
        err: null,
        action: action.type,
      };

    case AUTH_FETCH_SUCCESS:
      return {
        ...state,
        fetch: false,
        res: action.res || null,
        err: null,
        action: action.type,
      };

    case AUTH_FETCH_FAILED:
      return {
        ...state,
        fetch: false,
        err: action.err || null,
        action: action.type,
      };

    default:
      return state;
  };
};
