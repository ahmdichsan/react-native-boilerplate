/** put action type and action function here to make it easier to read */
import { LoginPayload, Auth } from '../../../../interfaces';

export const AUTH_FETCH = 'AUTH_FETCH';
export const AUTH_FETCH_SUCCESS = 'AUTH_FETCH_SUCCESS';
export const AUTH_FETCH_FAILED = 'AUTH_FETCH_FAILED';
export const fetchAuth = (data: LoginPayload) => ({ type: AUTH_FETCH, data });
export const fetchAuthSuccess = (res: Auth) => ({ type: AUTH_FETCH_SUCCESS, res });
export const fetchAuthFailed = (err: string) => ({ type: AUTH_FETCH_FAILED, err });
