/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React, { useEffect } from 'react';
import { ScrollView, StatusBar, KeyboardAvoidingView, View } from 'react-native';
import { SafeAreaView } from 'react-navigation';

/** Region Import Constants */
import { defaultKeyboardAvoidingViewProps } from '../../constants';

/** Region Import Interfaces */
import { LoginProps } from './interfaces';

/** Region Import Redux Action Type and Redux Action */

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */
import { useAuthSelector } from '../../custom-hook';

/** Region Import Components/Cards */
import { LoginForm } from './cards';

/** Region Import Assets */

/** Region Import Style */
import LoginStyle from './styles';
import { colors } from '../../styles';

function Login(props: LoginProps) {
  /** useDispatch declaration */

  /** useState declaration */

  /** props object destruction */

  /** useCustomHook */
  const { authRes } = useAuthSelector();

  /** onHandleFunction declaration */

  /** useEffect declaration */
  useEffect(() => {
    if (authRes) props.navigation.navigate('App');
  }, [authRes]);

  /** return element */
  return (
    <SafeAreaView style={[LoginStyle.container]}>
      <StatusBar barStyle="light-content" backgroundColor={colors.blueCompany} />
      <KeyboardAvoidingView
        {...defaultKeyboardAvoidingViewProps}
      >
        <ScrollView style={[LoginStyle.scrollView]} contentContainerStyle={[LoginStyle.scrollViewContainer]} keyboardShouldPersistTaps="handled">
          <View style={[{ paddingVertical: '50%' }]}>
            <LoginForm />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default Login;
