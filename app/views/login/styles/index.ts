import { StyleSheet } from 'react-native';
import { colors } from '../../../styles';

export default StyleSheet.create({
  container: {
    backgroundColor: colors.blueCompany,
    flex: 1,
  },
  scrollView: {
    flex: 1,
  },
  scrollViewContainer: {
    
  },
  textBase: {
    marginBottom: 4,
    marginTop: 4,
  },
  defaultMargin: {
    marginTop: 8,
    marginBottom: 8,
  }
});
