import { StyleSheet } from 'react-native';
import { colors } from '../../../../styles';

export default StyleSheet.create({
  textBase: {
    marginBottom: 4,
    marginTop: 4,
  },
  errorLogin: {
    color: colors.red,
  },
  defaultMargin: {
    marginTop: 8,
    marginBottom: 8,
  }
});
