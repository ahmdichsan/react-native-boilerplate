/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React, { useEffect } from 'react';
import { withNavigation } from 'react-navigation';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';

/** Region Import Constants */

/** Region Import Interfaces */
import { LoginFormProps } from './LoginForm.interfaces';

/** Region Import Redux Action Type and Redux Action */
import { fetchAuth, AUTH_FETCH_SUCCESS, AUTH_FETCH } from '../../redux/actions';

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */
import { useInputHook, useAuthSelector, usePrevious } from '../../../../custom-hook';

/** Region Import Components/Cards */
import { CardBase } from '../../../../cards';
import { ButtonDefault, TextBase } from '../../../../components';

/** Region Import Assets */

/** Region Import Style */
import LoginFormStyle from './LoginForm.styles';

function LoginForm(props: LoginFormProps) {
  /** useDispatch declaration */
  const dispatch = useDispatch();

  /** useState declaration */

  /** props object destruction */

  /** useCustomHook */
  const { Input: UserName, value: userName } = useInputHook({
    placeholder: 'Username',
  });

  const { Input: Password, value: password } = useInputHook({
    placeholder: 'Password',
    secureTextEntry: true,
  });

  const { authFetch, authAction, authErr } = useAuthSelector();

  const previousAuthAction = usePrevious(authAction);

  /** onHandleFunction declaration */
  function onSubmit() {
    dispatch(fetchAuth({ userName, password }));
  };

  function onLoginSuccess() {
    props.navigation.navigate('App');
  };

  /** useEffect declaration */
  useEffect(() => {
    if (previousAuthAction === AUTH_FETCH && authAction === AUTH_FETCH_SUCCESS) onLoginSuccess();
  }, [authAction]);

  /** return element */
  return (
    <CardBase containerStyle={[{ marginHorizontal: 10 }]}>
      <View>
        <TextBase style={[LoginFormStyle.textBase]}>
          Signs In to your account
        </TextBase>
        {authErr && (
          <TextBase style={[LoginFormStyle.textBase, LoginFormStyle.errorLogin]}>
            Login Failed: {authErr}
          </TextBase>
        )}
        <View style={[LoginFormStyle.defaultMargin]}>
          {UserName}
        </View>
        <View style={[LoginFormStyle.defaultMargin]}>
          {Password}
        </View>
        <ButtonDefault
          onPress={onSubmit}
          disabled={authFetch}
          isLoading={authFetch}
          text="Signs In"
          containerStyle={[LoginFormStyle.defaultMargin]}
        />
      </View>
    </CardBase>
  );
};

/**
 * using withNavigation to make this child component could access navigation props
 * remove it if not needed
*/
export default withNavigation(LoginForm);
