import { DefaultReduxAction, DefaultReduxState } from '../../../../interfaces';
import { defaultAction, defaultState } from '../../../../constants';
import {
  TO_DO_FETCH, TO_DO_FETCH_SUCCESS, TO_DO_FETCH_FAILED, UPDATE_TO_DO,
  ADD_TO_DO_FETCH, ADD_TO_DO_SUCCESS, ADD_TO_DO_FAILED,
  MARK_AS_DONE_FETCH, MARK_AS_DONE_SUCCESS, MARK_AS_DONE_FAILED,
  SYNC_TO_DO_FETCH, SYNC_TO_DO_SUCCESS, SYNC_TO_DO_FAILED,
} from '../actions';
import { ToDoData } from '../../interfaces';
import { ToDoSelector, AddToDoSelector, MarkAsDoneSelector } from '../../../../custom-hook';

export function ReducerToDo(state: ToDoSelector = defaultState, action: DefaultReduxAction<null, ToDoData[] | null> = defaultAction) {
  switch (action.type) {
    case TO_DO_FETCH:
      return {
        ...state,
        fetch: true,
        action: action.type,
      };

    case TO_DO_FETCH_SUCCESS:
    case TO_DO_FETCH_FAILED:
      return {
        ...state,
        fetch: false,
        action: action.type,
      };

    case UPDATE_TO_DO:
      return {
        ...state,
        res: action.res || null,
        action: action.type,
      };

    default:
      return state;
  }
}

export function ReducerAddToDo(state: AddToDoSelector = defaultState, action: DefaultReduxAction<string | null> = defaultAction) {
  switch (action.type) {
    case ADD_TO_DO_FETCH:
      return {
        ...state,
        fetch: true,
        data: action.data || null,
        action: action.type,
      };

    case ADD_TO_DO_SUCCESS:
    case ADD_TO_DO_FAILED:
      return {
        ...state,
        fetch: false,
        action: action.type,
      };

    default:
      return state;
  }
}

export function ReducerMarkAsDoneToDo(state: MarkAsDoneSelector = defaultState, action: DefaultReduxAction<ToDoData | string | null> = defaultAction) {
  switch (action.type) {
    case MARK_AS_DONE_FETCH:
      return {
        ...state,
        fetch: true,
        data: action.data as ToDoData,
        action: action.type,
      };

    case MARK_AS_DONE_SUCCESS:
      return {
        ...state,
        res: action.data as string,
        fetch: false,
        action: action.type,
      };

    case MARK_AS_DONE_FAILED:
      return {
        ...state,
        fetch: false,
        action: action.type,
      };

    default:
      return state;
  }
}

export function ReducerSyncToDo(state: DefaultReduxState = defaultState, action: DefaultReduxAction = defaultAction) {
  switch (action.type) {
    case SYNC_TO_DO_FETCH:
      return {
        ...state,
        fetch: true,
        action: action.type,
      };

    case SYNC_TO_DO_SUCCESS:
    case SYNC_TO_DO_FAILED:
      return {
        ...state,
        fetch: false,
        action: action.type,
      };

    default:
      return state;
  }
}
