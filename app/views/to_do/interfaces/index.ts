import { NavigationProps } from '../../../interfaces';

export interface ToDoProps extends NavigationProps { };

export interface ToDoState {
  isLoading: boolean;
};

export declare interface ToDoData {
  createdBy: string;
  createdDate: string;
  lastUpdatedBy: string | null;
  lastUpdatedDate: string | null;
  activeFlag?: boolean;
  id: string;
  toDo: string;
  isToDoDone: boolean;
  isSynced: boolean;
};
