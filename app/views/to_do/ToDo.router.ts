import ToDO from './ToDo';

const ToDoRoutes = {
  ToDo: { screen: ToDO },
};

export default ToDoRoutes;
