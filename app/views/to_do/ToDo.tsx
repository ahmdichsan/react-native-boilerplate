/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React, { useState, useEffect } from 'react';
import { StatusBar, ScrollView, View, KeyboardAvoidingView, RefreshControl } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import SegmentedControlTab from "react-native-segmented-control-tab";
import { useDispatch } from 'react-redux';
import { NavigationEvents } from 'react-navigation';

/** Region Import Constants */
import { defaultKeyboardAvoidingViewProps } from '../../constants';

/** Region Import Interfaces */
import { ToDoProps, ToDoData } from './interfaces';

/** Region Import Redux Action Type and Redux Action */
import {
  addToDoFetch, toDoFetch, ADD_TO_DO_FETCH,
  ADD_TO_DO_SUCCESS, TO_DO_FETCH, TO_DO_FETCH_SUCCESS,
  TO_DO_FETCH_FAILED, ADD_TO_DO_FAILED,
} from './redux/actions';

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */
import { useInputHook, useToDoSelector, useAddToDoSelector, usePrevious } from '../../custom-hook';

/** Region Import Components/Cards */
import { TextBase, ButtonDefault } from '../../components';
import { ToDoItem } from './components';
import { CardBase } from '../../cards';

/** Region Import Assets */

/** Region Import Style */
import ToDoStyles from './styles';
import { colors } from '../../styles';

function ToDo(props: ToDoProps) {
  /** useDispatch declaration */
  const dispatch = useDispatch();

  /** useState declaration */
  const [activeTab, setActiveTab] = useState(0);
  const [unDoneToDo, setUnDoneToDo] = useState<ToDoData[]>([]);
  const [doneToDo, setDoneToDo] = useState<ToDoData[]>([]);
  const [toDoValue, setToDoValue] = useState('');
  const [isInitialLoad, setIsInitialLoad] = useState(true);

  /** props object destruction */

  /** useCustomHook */
  const { Input: ToDoInput } = useInputHook({
    placeholder: 'Create your to do here...',
    style: {
      backgroundColor: colors.white
    },
    value: toDoValue,
    onChangeText: setToDoValue,
  });

  const { resToDo, actionToDo, fetchToDo } = useToDoSelector();
  const previousActionToDo = usePrevious(actionToDo);

  const { fetchAddToDo, actionAddToDo } = useAddToDoSelector();
  const previousActionAddToDo = usePrevious(actionAddToDo);

  /** onHandleFunction declaration */
  function onAddToDo() {
    dispatch(addToDoFetch(toDoValue));
  };

  function getToDo() {
    dispatch(toDoFetch());
  };

  function onScreenFocus() {
    setIsInitialLoad(true);
  };

  function onScreenBlured() {
    setIsInitialLoad(false);
  };

  /** useEffect declaration */
  useEffect(() => {
    if (isInitialLoad) getToDo();
  }, [isInitialLoad]);

  useEffect(() => {
    if (isInitialLoad && previousActionToDo === TO_DO_FETCH && (actionToDo === TO_DO_FETCH_SUCCESS || actionToDo === TO_DO_FETCH_FAILED)) {
      setIsInitialLoad(false);
    }
  }, [previousActionToDo, actionToDo]);

  useEffect(() => {
    if (previousActionAddToDo === ADD_TO_DO_FETCH && (actionAddToDo === ADD_TO_DO_SUCCESS || actionAddToDo === ADD_TO_DO_FAILED)) {
      setToDoValue('');
    }
  }, [previousActionAddToDo, actionAddToDo]);

  useEffect(() => {
    if (!resToDo) return;

    const resultUnDoneToDo = resToDo.filter(item => !item.isToDoDone);
    const resultDoneToDo = resToDo.filter(item => item.isToDoDone);

    setUnDoneToDo(resultUnDoneToDo);
    setDoneToDo(resultDoneToDo);
  }, [resToDo]);

  /** return element */
  return (
    <LinearGradient
      colors={[colors.blueCompany, colors.white]}
      style={[ToDoStyles.linearGradient]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >
      <SafeAreaView style={[ToDoStyles.container]} forceInset={{ top: 'always' }}>
        <StatusBar barStyle="light-content" translucent backgroundColor="transparent" />
        <KeyboardAvoidingView
          {...defaultKeyboardAvoidingViewProps}
        >
          <NavigationEvents onDidFocus={onScreenFocus} onDidBlur={onScreenBlured} />
          <ScrollView
            style={[ToDoStyles.scrollView]}
            keyboardShouldPersistTaps="handled"
            refreshControl={<RefreshControl refreshing={!isInitialLoad && fetchToDo} onRefresh={getToDo} />}
          >
            <View style={[ToDoStyles.contentWrapper]}>
              <View style={[ToDoStyles.viewWrapper]}>
                <TextBase style={[ToDoStyles.text]}>
                  Add to do
                </TextBase>
              </View>
              <View style={[ToDoStyles.viewWrapper]}>
                {ToDoInput}
              </View>
              <View style={[ToDoStyles.buttonAddWrapper]}>
                <ButtonDefault
                  text="Add"
                  onPress={onAddToDo}
                  containerStyle={[ToDoStyles.buttonAdd]}
                  disabled={fetchAddToDo}
                  isLoading={fetchAddToDo}
                  iconProps={{
                    icon: faPlusCircle,
                  }}
                />
              </View>
              <View style={[ToDoStyles.tabHeader]}>
                <SegmentedControlTab
                  values={["To Do", "Done"]}
                  selectedIndex={activeTab}
                  onTabPress={setActiveTab}
                  activeTabStyle={[ToDoStyles.activeTabStyle]}
                  activeTabTextStyle={[ToDoStyles.activeTabTextStyle]}
                  tabStyle={[ToDoStyles.tabStyle]}
                  tabTextStyle={[ToDoStyles.tabTextStyle]}
                  tabsContainerStyle={[ToDoStyles.tabsContainerStyle]}
                />
              </View>
              {activeTab === 0 && (
                <View style={[ToDoStyles.viewWrapperTabContent]}>
                  <CardBase containerStyle={[ToDoStyles.cardTab]}>
                    <>
                      {unDoneToDo.length > 0 && unDoneToDo.map((item: ToDoData) => <ToDoItem toDo={item} key={item.id} />)}

                      {unDoneToDo.length === 0 && (
                        <TextBase style={[ToDoStyles.textCardItem]}>
                          You have nothing to do. Let's make one!
                        </TextBase>
                      )}
                    </>
                  </CardBase>
                </View>
              )}
              {activeTab === 1 && (
                <View style={[ToDoStyles.viewWrapperTabContent]}>
                  <CardBase containerStyle={[ToDoStyles.cardTab]}>
                    <>
                      {doneToDo.length > 0 && doneToDo.map((item: ToDoData) => <ToDoItem toDo={item} key={item.id} />)}

                      {doneToDo.length === 0 && (
                        <TextBase style={[ToDoStyles.textCardItem]}>
                          Nothings done yet. Lets get all task in To Do done
                        </TextBase>
                      )}
                    </>
                  </CardBase>
                </View>
              )}
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </LinearGradient>
  );
};

export default ToDo;
