import { StyleSheet, StatusBar } from 'react-native';
import { colors } from '../../../styles';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
  },
  linearGradient: {
    paddingTop: StatusBar.currentHeight,
    flex: 1,
  },
  text: {
    color: colors.white,
    paddingTop: 0,
  },
  contentWrapper: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    flex: 1,
  },
  tabHeader: {
    marginTop: 40,
  },
  buttonAddWrapper: {
    marginVertical: 3,
    alignItems: 'flex-end',
  },
  buttonAdd: {
    width: 80
  },
  viewWrapper: {
    marginBottom: 3,
  },
  viewWrapperTabContent: {
    marginTop: 15,
  },
  cardTab: {
    minHeight: 250,
  },
  textCardItem: {
    textAlign: 'center',
  },
  activeTabStyle: {
    backgroundColor: colors.blueCompany
  },
  activeTabTextStyle: {
    color: colors.yellowCompany,
  },
  tabStyle: {
    backgroundColor: colors.yellowCompany,
    borderColor: colors.yellowCompany,
  },
  tabTextStyle: {
    color: colors.blueCompany,
  },
  tabsContainerStyle: {
    borderRadius: 5,
    height: 40,
  },
});
