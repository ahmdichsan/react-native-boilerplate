import { ToDoData } from "../../interfaces";

export declare interface ToDoItemProps {
  toDo: ToDoData | null;
};
