import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    cardContainer: {
        marginBottom: 15,
    },
    viewWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    buttonAction: {
        width: 60,
    },
});
