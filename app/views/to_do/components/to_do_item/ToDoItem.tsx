/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { View } from 'react-native';
import { faTrash, faCheckCircle } from '@fortawesome/free-solid-svg-icons';

/** Region Import Constants */

/** Region Import Interfaces */
import { ToDoItemProps } from './ToDoItem.interfaces';

/** Region Import Redux Action Type and Redux Action */
import { markAsDoneFetch, toDoFetch } from '../../redux/actions';

/** Region Import Utility/Helper Function */
import { HttpService } from '../../../../helpers';

/** Region Import Custom Hook */
import { useMarkAsDoneToDoSelector, useNetworkSelector } from '../../../../custom-hook';

/** Region Import Components/Cards */
import { CardBase } from '../../../../cards';
import { TextBase, ButtonDefault } from '../../../../components';

/** Region Import Assets */

/** Region Import Style */
import ToDoItemStyles from './ToDoItem.styles';

export default function ToDoItem(props: ToDoItemProps) {
  /** useDispatch declaration */
  const dispatch = useDispatch();

  /** useState declaration */
  const [isDeleteDisabled, setIsDeleteDisabled] = useState(false);

  /** props object destruction */
  const { toDo } = props;

  /** useCustomHook */
  const { fetchMarkAsDoneToDo, dataMarkAsDoneToDo } = useMarkAsDoneToDoSelector();
  const { isOnline } = useNetworkSelector();

  /** onHandleFunction declaration */
  function markAsDone() {
    try {
      if (!toDo) return;

      dispatch(markAsDoneFetch(toDo));
    } catch (error) {
      const errorMessage = `error markAsDone: ${error.message}`;
      console.log(errorMessage);
    }
  };

  async function deleteToDo() {
    try {
      if (!props.toDo) return;

      setIsDeleteDisabled(true);

      await HttpService.delete(`todo/${props.toDo.id}`);

      setIsDeleteDisabled(false);

      dispatch(toDoFetch());
    } catch (error) {
      const errorMessage = `error deleteToDo: ${error.message}`;
      console.log(errorMessage);
    }
  };

  /** useEffect declaration */

  /** return element */
  if (!toDo) return null;

  return (
    <CardBase containerStyle={[ToDoItemStyles.cardContainer]}>
      <View style={[ToDoItemStyles.viewWrapper]}>
        <View>
          <TextBase>
            {`${toDo.toDo}${!toDo.isSynced ? ' [Not Synced]' : ''}`}
          </TextBase>
        </View>
        {!toDo.isToDoDone && (
          <>
            <ButtonDefault
              onPress={markAsDone}
              isLoading={dataMarkAsDoneToDo?.id === toDo.id && fetchMarkAsDoneToDo}
              disabled={dataMarkAsDoneToDo?.id === toDo.id && fetchMarkAsDoneToDo}
              iconProps={{
                icon: faCheckCircle,
                iconSize: 20,
              }}
              containerStyle={[ToDoItemStyles.buttonAction]}
              text=""
            />
          </>
        )}
        {toDo.isToDoDone && (
          <>
            <ButtonDefault
              onPress={deleteToDo}
              disabled={!isOnline || isDeleteDisabled}
              isLoading={isDeleteDisabled}
              iconProps={{
                icon: faTrash,
                iconSize: 20,
              }}
              containerStyle={[ToDoItemStyles.buttonAction]}
              text=""
            />
          </>
        )}
      </View>
    </CardBase>
  );
};
