/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React from 'react';
import { StatusBar, ScrollView } from 'react-native';
import { SafeAreaView } from 'react-navigation';

/** Region Import Constants */

/** Region Import Interfaces */
import { ExampleProps } from './interfaces';

/** Region Import Redux Action Type and Redux Action */

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */

/** Region Import Components/Cards */
import { ExampleComponent } from './components';

/** Region Import Assets */

/** Region Import Style */
import ExampleStyle from './styles';

function Example(props: ExampleProps) {
  /** useDispatch declaration */

  /** useState declaration */

  /** props object destruction */

  /** useCustomHook */

  /** onHandleFunction declaration */

  /** useEffect declaration */

  /** return element */
  return (
    <SafeAreaView style={[ExampleStyle.container]}>
      <StatusBar barStyle="dark-content" />
      <ScrollView>
        <ExampleComponent />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Example;
