import { StyleSheet } from 'react-native';
import { colors } from '../../../styles';

export default StyleSheet.create({
  container: {
    backgroundColor: colors.blueCompany,
    flex: 1,
  },
});
