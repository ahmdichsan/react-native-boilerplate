import Example from './Example';

const ExampleRoutes = {
  Example: { screen: Example },
};

export default ExampleRoutes;
