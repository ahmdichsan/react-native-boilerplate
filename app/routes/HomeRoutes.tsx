import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';

import { CustomNavigation, StackConfiguration } from '../interfaces';

import { colors } from '../styles';
import { tabBarStatus } from './Routes.helpers';

import HomeRoutes from '../views/home/Home.router';
import HomeDetailRoutes from '../views/home_detail/HomeDetail.router';

const homeStackConfig: StackConfiguration = {
    initialRouteName: 'Home',
    headerMode: 'none',
};

export const homeStackNavigation = createStackNavigator(
    {
        ...HomeRoutes,
        ...HomeDetailRoutes,
    },
    homeStackConfig,
);

export const homeNavConfig = {
    navigationOptions: ({ navigation }: CustomNavigation) => {
        const { isActive, isTabBarVisible } = tabBarStatus({ navigation });

        return {
            tabBarVisible: isTabBarVisible,
            tabBarIcon: () => <FontAwesomeIcon icon={faHome} color={isActive ? colors.yellowCompany : colors.white} size={20} />,
        };
    },
};
