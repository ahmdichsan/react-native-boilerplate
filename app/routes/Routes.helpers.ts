import { CustomNavigation } from '../interfaces';

export function tabBarStatus({ navigation }: CustomNavigation) {
    const { index = 0 } = navigation.state;

    const isActive = navigation.isFocused();

    let isTabBarVisible = true;

    if (index > 0) isTabBarVisible = false;

    return {
        isActive,
        isTabBarVisible,
    }
};
