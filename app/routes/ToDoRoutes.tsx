import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faTasks } from '@fortawesome/free-solid-svg-icons';

import { CustomNavigation, StackConfiguration } from '../interfaces';

import { colors } from '../styles';
import { tabBarStatus } from './Routes.helpers';

import ToDoRoutes from '../views/to_do/ToDo.router';

const toDoStackConfig: StackConfiguration = {
    initialRouteName: 'ToDo',
    headerMode: 'none',
};

export const toDoStackNavigation = createStackNavigator(
    {
        ...ToDoRoutes,
    },
    toDoStackConfig,
);

export const toDoNavConfig = {
    navigationOptions: ({ navigation }: CustomNavigation) => {
        const { isActive, isTabBarVisible } = tabBarStatus({ navigation });

        return {
            tabBarVisible: isTabBarVisible,
            tabBarIcon: () => <FontAwesomeIcon icon={faTasks} color={isActive ? colors.yellowCompany : colors.white} size={20} />,
        };
    },
};
