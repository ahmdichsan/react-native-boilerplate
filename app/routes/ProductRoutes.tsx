import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faBoxOpen } from '@fortawesome/free-solid-svg-icons';

import { CustomNavigation, StackConfiguration } from '../interfaces';

import { colors } from '../styles';
import { tabBarStatus } from './Routes.helpers';

import ProductRoutes from '../views/product/Product.router';

const productStackConfig: StackConfiguration = {
    initialRouteName: 'Product',
    headerMode: 'none',
};

export const productStackNavigation = createStackNavigator(
    {
        ...ProductRoutes,
    },
    productStackConfig,
);

export const productNavConfig = {
    navigationOptions: ({ navigation }: CustomNavigation) => {
        const { isActive, isTabBarVisible } = tabBarStatus({ navigation });

        return {
            tabBarVisible: isTabBarVisible,
            tabBarIcon: () => <FontAwesomeIcon icon={faBoxOpen} color={isActive ? colors.yellowCompany : colors.white} size={20} />,
        };
    },
};
