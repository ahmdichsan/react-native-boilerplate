import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';

import { CustomNavigation, StackConfiguration } from '../interfaces';

import { colors } from '../styles';
import { tabBarStatus } from './Routes.helpers';

import ProfileRoutes from '../views/profile/Profile.router';

const profileStackConfig: StackConfiguration = {
    initialRouteName: 'Profile',
    headerMode: 'none',
};

export const profileStackNavigation = createStackNavigator(
    {
        ...ProfileRoutes,
    },
    profileStackConfig,
);

export const profileNavConfig = {
    navigationOptions: ({ navigation }: CustomNavigation) => {
        const { isActive, isTabBarVisible } = tabBarStatus({ navigation });

        return {
            tabBarVisible: isTabBarVisible,
            tabBarIcon: () => <FontAwesomeIcon icon={faUser} color={isActive ? colors.yellowCompany : colors.white} size={20} />,
        };
    },
};
