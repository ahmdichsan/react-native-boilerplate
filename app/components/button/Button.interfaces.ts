import { StyleProp, ViewStyle, TextStyle, TouchableOpacityProps } from 'react-native';
import { TextBaseProps } from '../text_base/TextBase.interfaces';
import { Key } from '../../interfaces';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIconStyle } from '@fortawesome/react-native-fontawesome';

export interface DefaultButton extends Key {
  onPress: () => void;
  text: string;
  containerStyle?: StyleProp<ViewStyle>;
  disabled?: boolean;
  textStyle?: StyleProp<TextStyle>;
  touchableOpacityProps?: TouchableOpacityProps;
  textProps?: TextBaseProps;
  textType?: 'CAPITALIZE' | 'LOWERCASE' | 'DEFAULT';
  isLoading?: boolean;
  loadingColor?: string;
  iconProps?: {
    icon: IconDefinition;
    iconPosition?: 'LEFT' | 'RIGHT';
    iconColor?: string;
    iconSize?: number;
    iconStyle?: FontAwesomeIconStyle;
  }
};

export interface DangerButton extends DefaultButton { };
