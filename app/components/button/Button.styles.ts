import { StyleSheet } from 'react-native';
import { colors } from '../../styles';

export default StyleSheet.create({
  buttonContainer: {
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 40,
  },
  normal: {
    backgroundColor: colors.blueCompany,
  },
  danger: {
    backgroundColor: colors.maroon,
  },
  text: {
    fontSize: 14,
    color: colors.yellowCompany,
    fontWeight: 'bold',
  },
  icon: {
    marginHorizontal: 4,
  },
  contentWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
