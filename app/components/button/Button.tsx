/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React from 'react';
import { TouchableOpacity, ActivityIndicator, View } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

/** Region Import Constants */

/** Region Import Interfaces */
import { DefaultButton, DangerButton } from './Button.interfaces';

/** Region Import Redux Action Type and Redux Action */

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */

/** Region Import Components/Cards */
import { TextBase } from '../text_base';

/** Region Import Assets */

/** Region Import Style */
import StyleButton from './Button.styles';
import { colors } from '../../styles';

export function ButtonDefault(props: DefaultButton) {
  /** useDispatch declaration */

  /** useState declaration */

  /** props object destruction */
  const {
    onPress, text, containerStyle,
    textStyle, disabled, textProps,
    touchableOpacityProps, textType = 'DEFAULT', isLoading,
    loadingColor, iconProps,
  } = props;

  const iconPosition = iconProps?.iconPosition || 'LEFT';

  let buttonText = text;

  switch (textType) {
    case 'CAPITALIZE':
      buttonText = text.toUpperCase();
      break;
    case 'LOWERCASE':
      buttonText = text.toLowerCase();
      break;

    default:
      break;
  };

  /** useCustomHook */

  /** onHandleFunction declaration */

  /** useEffect declaration */

  /** return element */
  return (
    <TouchableOpacity
      {...touchableOpacityProps}
      disabled={disabled || false}
      onPress={onPress}
      style={[StyleButton.buttonContainer, StyleButton.normal, containerStyle]}
    >
      {!isLoading && (
        <View style={[StyleButton.contentWrapper]}>
          {iconProps && iconPosition === 'LEFT' && (
            <FontAwesomeIcon
              icon={iconProps.icon}
              color={iconProps.iconColor || colors.white}
              size={iconProps.iconSize || 20}
              style={[StyleButton.icon, iconProps.iconStyle]}
            />
          )}
          <TextBase {...textProps} style={[StyleButton.text, textStyle]}>
            {buttonText}
          </TextBase>
          {iconProps && iconPosition === 'RIGHT' && (
            <FontAwesomeIcon
              icon={iconProps.icon}
              color={iconProps.iconColor || colors.white}
              size={iconProps.iconSize || 20}
              style={[StyleButton.icon, iconProps.iconStyle]}
            />
          )}
        </View>
      )}
      {isLoading && (
        <ActivityIndicator size="small" color={loadingColor || colors.yellowCompany} />
      )}
    </TouchableOpacity>
  );
};

export function ButtonDanger({ containerStyle, ...rest }: DangerButton) {
  /** useDispatch declaration */

  /** useState declaration */

  /** props object destruction */

  /** useCustomHook */

  /** onHandleFunction declaration */

  /** useEffect declaration */

  /** return element */
  return <ButtonDefault containerStyle={[StyleButton.danger, containerStyle]} {...rest} />;
};
