/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React from 'react';
import { Text } from 'react-native';

/** Region Import Constants */

/** Region Import Interfaces */
import { TextBaseProps } from './TextBase.interfaces';

/** Region Import Redux Action Type and Redux Action */

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */

/** Region Import Components/Cards */

/** Region Import Assets */

/** Region Import Style */
import TextBaseStyle from './TextBase.styles';

function TextBase(props: TextBaseProps) {
  /** useDispatch declaration */

  /** useState declaration */

  /** props object destruction */

  /** useCustomHook */

  /** onHandleFunction declaration */

  /** useEffect declaration */

  /** return element */
  return (
    <Text {...props} style={[TextBaseStyle.textStyle, props.style]}>
      {props.children}
    </Text>
  )
}

export default TextBase;
