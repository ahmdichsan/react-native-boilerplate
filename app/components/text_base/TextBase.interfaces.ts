import { TextProps } from 'react-native';
import { Key } from '../../interfaces';

export interface TextBaseProps extends TextProps, Key {
    children: React.ReactNode;
};

export interface TextBaseState { };
