import { StyleSheet } from 'react-native';
import { colors } from '../../styles';

export default StyleSheet.create({
  textStyle: {
    fontSize: 14,
    color: colors.black,
    paddingTop: 4,
    paddingBottom: 4
  },
});
