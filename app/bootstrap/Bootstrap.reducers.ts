import { combineReducers } from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import ConfigReducers from '../configs/Config.reducers';
import { DefaultReduxAction } from '../interfaces';
import { USER_LOG_OUT } from '../redux/Global.actions';

function hookReducer(state: any, action: DefaultReduxAction) {
    // do your hook here
    if (action.type === USER_LOG_OUT) {
        AsyncStorage.removeItem('persist:root');
        state = undefined;
    };

    return { state, action };
};

const reducers = combineReducers(ConfigReducers);

export type ReduxAppState = ReturnType<typeof reducers>;

function bootstrapReducers(state: any, action: DefaultReduxAction) {
    const hook = hookReducer(state, action);
    return reducers(hook.state, hook.action);
};

export default bootstrapReducers;
