import React, { useEffect } from 'react';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';
import { AppRoutes } from '../configs';
import { usePrevious, useIsOnline } from '../custom-hook';
import { updateNetwork } from '../redux/Global.actions';
import { syncToDoFetch } from '../views/to_do/redux/actions';
import { styles } from '../styles';

function Container() {
    const dispatch = useDispatch();

    const isOnline = useIsOnline();
    const previousOnlineStatus = usePrevious(isOnline);

    useEffect(() => {
        if (previousOnlineStatus !== isOnline) {
            dispatch(updateNetwork(isOnline));
        }

        // call sync
        if (!previousOnlineStatus && isOnline) {
            dispatch(syncToDoFetch());
        }

    }, [isOnline, previousOnlineStatus])

    return (
        <View style={[styles.appWrapper]}>
            <AppRoutes />
        </View>
    );
};

export default Container;
