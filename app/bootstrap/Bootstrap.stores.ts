import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';
import { Persistor } from 'redux-persist/es/types';
import ConfigPersists from '../configs/Config.persists';
import BootstrapReducers from './Bootstrap.reducers';
import BootstrapSagas from './Bootstrap.sagas';

const finalReducers = persistReducer({ ...ConfigPersists }, BootstrapReducers);
const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];

const reduxStore = createStore(
    finalReducers,
    compose(applyMiddleware(...middlewares)),
);

sagaMiddleware.run(BootstrapSagas);

const persistor = persistStore(reduxStore) as unknown as Persistor;
const store = reduxStore;

export {
    persistor,
    store,
};
