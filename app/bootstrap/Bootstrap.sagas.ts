import { all } from 'redux-saga/effects';
import ConfigSagas from '../configs/Config.sagas';

export default function* bootstrapSagas() {
  yield all(ConfigSagas);
};
