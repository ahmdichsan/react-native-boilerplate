import { ForkEffect } from 'redux-saga/effects';

import { watcherAuth } from '../views/login/redux/sagas/Login.sagas';
import { watcherSagaToDo } from '../views/to_do/redux/sagas/ToDo.sagas';

const configSaga: ForkEffect<never>[] = [
    ...watcherAuth,
    ...watcherSagaToDo,
];

export default configSaga;
