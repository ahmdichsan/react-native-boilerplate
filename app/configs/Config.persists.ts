import AsyncStorage from '@react-native-community/async-storage';
import createEncryptor from 'redux-persist-transform-encrypt';
import { createBlacklistFilter } from 'redux-persist-transform-filter';
import { PersistConfig } from 'redux-persist';

const encryptor: any = createEncryptor({
    secretKey: 'my-super-secret-key',
    onError(error) {
        console.log('createEncryptor error ', error);
    },
});

const saveAuthSubsetBlacklistFilter: any = createBlacklistFilter('auth', ['data']);

const configPersists: PersistConfig<any> = {
    key: 'root',
    storage: AsyncStorage,
    transforms: [encryptor, saveAuthSubsetBlacklistFilter],
};

export default configPersists;
