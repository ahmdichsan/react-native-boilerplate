import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import LoginRoutes from '../views/login/Login.router';
import {
    homeNavConfig, homeStackNavigation,
    productNavConfig, productStackNavigation,
    toDoNavConfig, toDoStackNavigation,
    profileNavConfig, profileStackNavigation,
} from '../routes';
import { colors } from '../styles';

const App = createBottomTabNavigator(
    {
        Home: {
            screen: homeStackNavigation,
            ...homeNavConfig,
        },
        Product: {
            screen: productStackNavigation,
            ...productNavConfig,
        },
        ToDo: {
            screen: toDoStackNavigation,
            ...toDoNavConfig,
        },
        Profile: {
            screen: profileStackNavigation,
            ...profileNavConfig,
        },
    },
    {
        initialRouteName: 'Home',
        tabBarOptions: {
            showLabel: false,
            inactiveBackgroundColor: colors.blueCompany,
            activeBackgroundColor: colors.blueCompany,
            keyboardHidesTabBar: false,
        },
    },
);

const Login = createStackNavigator(LoginRoutes, { initialRouteName: 'Login', headerMode: 'none' });

export const AppRoutes = createAppContainer(
    createSwitchNavigator(
        {
            Login,
            App,
        },
    ),
);