import { ReducerAuth } from '../views/login/redux/reducers';
import { ReducerNetwork } from '../redux/Network.reducers';
import { ReducerAddToDo, ReducerMarkAsDoneToDo, ReducerSyncToDo, ReducerToDo } from '../views/to_do/redux/reducers';

const configReducer = {
    auth: ReducerAuth,
    network: ReducerNetwork,
    toDo: ReducerToDo,
    addToDo: ReducerAddToDo,
    markAsDoneToDo: ReducerMarkAsDoneToDo,
    syncToDo: ReducerSyncToDo,
};

export default configReducer;
