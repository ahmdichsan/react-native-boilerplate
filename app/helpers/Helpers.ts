import _ from 'lodash';
import { ToDoData } from '../views/to_do/interfaces';
import { ReduxAppState } from '../bootstrap/Bootstrap.reducers';

export function convertToSentenceText(value: string): string {
  return _.startCase(_.toLower(value));
}

export function greetingSentence(): string {
  const hour = new Date().getHours();

  if (hour >= 0 && hour < 12) return "Good morning";

  if (hour >= 12 && hour < 17) return "Good afternoon";

  return "Good evening";
};

export function getToDoState(state: ReduxAppState): ToDoData[] | null {
  return state.toDo.res;
};

export function getNetworkState(state: ReduxAppState): boolean | null {
  return state.network.res;
};

/**
 * @description
 * if you see this function called in saga, it is to make dispatch action not occur in short period of time.
 * this will give a little time to make useSelector get every dispatched action
 * 
 * @param timeout 
 */
export function waitMe(timeout: number = 0) {
  return new Promise<undefined>(resolve => {
    setTimeout(() => {
      resolve();
    }, timeout);
  });
};