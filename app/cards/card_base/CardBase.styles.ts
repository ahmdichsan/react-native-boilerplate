import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  default: {
    borderRadius: 5,
    margin: 0,
  },
});
