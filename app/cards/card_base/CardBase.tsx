/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React from 'react';
import { Card } from 'react-native-elements';

/** Region Import Constants */

/** Region Import Interfaces */
import { CardBaseProps } from './CardBase.interfaces';

/** Region Import Redux Action Type and Redux Action */

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */

/** Region Import Components/Cards */

/** Region Import Assets */

/** Region Import Style */
import CardBaseStyle from './CardBase.styles';

function CardBase(props: CardBaseProps) {
  /** useDispatch declaration */

  /** useState declaration */

  /** props object destruction */

  /** useCustomHook */

  /** onHandleFunction declaration */

  /** useEffect declaration */

  /** return element */
  return (
    <Card {...props} containerStyle={[CardBaseStyle.default, props.containerStyle]}>
      {props.children}
    </Card>
  );
};

export default CardBase;
