import { CardProps } from 'react-native-elements';
import { Key } from '../../interfaces';

export interface CardBaseProps extends CardProps, Key {
    children: JSX.Element;
};

export interface CardBaseState { };
