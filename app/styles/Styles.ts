import { StyleSheet } from 'react-native';
import colors from './Colors';

const styles = StyleSheet.create({
    backgroundCompany: {
        backgroundColor: colors.blueCompany,
        color: colors.white,
    },
    appWrapper: {
        flex: 1,
        margin: 0,
        padding: 0,
        backgroundColor: colors.white,
    },
});

export default styles;
