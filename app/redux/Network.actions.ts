export const UPDATE_NETWORK = 'UPDATE_NETWORK';

export const updateNetwork = (res: boolean) => ({ type: UPDATE_NETWORK, res });
