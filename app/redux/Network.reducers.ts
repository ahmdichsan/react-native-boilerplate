import { DefaultReduxState, DefaultReduxAction } from '../interfaces';
import { defaultState, defaultAction } from '../constants';
import { UPDATE_NETWORK } from './Network.actions';

export function ReducerNetwork(state: DefaultReduxState<null, boolean | null> = defaultState, action: DefaultReduxAction<null, boolean | null> = defaultAction): DefaultReduxState<null, boolean | null> {
  switch (action.type) {
    case UPDATE_NETWORK:
      return {
        ...state,
        res: action.res || null,
        action: action.type,
      };

    default:
      return state;
  };
};
