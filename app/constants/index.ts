import { KeyboardAvoidingViewProps, Platform } from 'react-native';
export * from './ReduxActionState';
export * from './ApiIdentity';

export const defaultKeyboardAvoidingViewProps: KeyboardAvoidingViewProps = {
    behavior: `${Platform.OS === 'ios' ? "padding" : "height"}` as 'height' | 'position' | 'padding',
    style: {
        flex: 1,
    }
};
