import { DefaultReduxAction, DefaultReduxState } from '../interfaces';

export const defaultState: DefaultReduxState = {
  action: '',
  err: null,
  data: null,
  fetch: false,
  res: null,
};

export const defaultAction: DefaultReduxAction = {
  type: '',
  data: null,
};
