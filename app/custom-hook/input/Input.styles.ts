import { StyleSheet } from 'react-native';
import { colors } from '../../styles';

export default StyleSheet.create({
  input: {
    color: colors.black,
    borderRadius: 5,
    borderColor: colors.gray,
    borderWidth: 0.5,
    padding: 10
  }
});
