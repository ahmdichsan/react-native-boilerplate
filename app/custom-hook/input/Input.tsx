/** Region Import External Lib (e.g React, Reactstrap, etc) */
import React, { useState } from 'react';
import { TextInput } from 'react-native';

/** Region Import Constants */

/** Region Import Interfaces */
import { InputProps } from './Input.interfaces';

/** Region Import Redux Action Type and Redux Action */

/** Region Import Utility/Helper Function */

/** Region Import Custom Hook */

/** Region Import Components/Cards */

/** Region Import Assets */

/** Region Import Style */
import InputStyle from './Input.styles';
import { colors } from '../../styles';

function useInputHook(props?: InputProps) {
  /** useDispatch declaration */

  /** useState declaration */
  const [state, setState] = useState<string>(props?.value || '');

  /** props object destruction */

  /** useCustomHook */

  /** onHandleFunction declaration */
  function onChangeText(text: string) {
    setState(text);
  }

  /** useEffect declaration */

  /** return element */
  return {
    Input: <TextInput
      {...props}
      placeholderTextColor={props?.placeholderTextColor || colors.gray}
      value={props?.value || state}
      onChangeText={props?.onChangeText || onChangeText}
      style={[InputStyle.input, props?.style]}
    />,
    value: state,
  }
}

export default useInputHook;
