import { TextInputProps } from 'react-native';
import { Key } from '../../interfaces';

export interface InputProps extends TextInputProps, Key { };

export interface InputState {
    state: string;
};
