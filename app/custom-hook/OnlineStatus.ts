import { useState, useEffect } from 'react';
import { useNetInfo } from '@react-native-community/netinfo';

export function useIsOnline(): boolean {
  const [isOnline, setIsOnline] = useState(false)
  const { isConnected, isInternetReachable } = useNetInfo();

  async function updateNetwork() {
    const condition = isConnected && isInternetReachable ? 'online' : 'offline';

    if (condition === 'online') {
      /**
       * try to ping google to make sure the connection exist or not
       */
      await fetch('https://google.com', { mode: 'no-cors' })
        .then(() => {
          setIsOnline(true);
        })
        .catch(() => {
          setIsOnline(false);
        });

      return;
    }

    setIsOnline(false);
  }

  useEffect(() => {
    updateNetwork();
  }, [isConnected, isInternetReachable]);

  return isOnline;
};
