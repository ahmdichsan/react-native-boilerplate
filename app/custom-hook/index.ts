import { useRef, useEffect } from 'react';

export * from './Selectors';
export * from './OnlineStatus';
export * from './AppState';
export * from './input';

export function usePrevious<T extends {}>(value: T): T | undefined {
  const ref = useRef<T>();

  useEffect(() => {
    ref.current = value;
  });

  return ref.current;
}
