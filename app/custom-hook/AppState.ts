import { AppState, AppStateStatus } from 'react-native';
import { useState, useEffect } from 'react';

export function appState(): AppStateStatus {
    const [appState, setAppState] = useState<AppStateStatus>('unknown');

    function handleAppStateChange(type: AppStateStatus) {
        setAppState(type);
    }

    useEffect(() => {
        AppState.addEventListener('change', handleAppStateChange);

        return () => {
            AppState.removeEventListener('change', handleAppStateChange);
        }
    }, []);

    return appState;
};
