import { useSelector } from 'react-redux';
import { ReduxAppState } from '../bootstrap/Bootstrap.reducers';
import { DefaultReduxState, LoginPayload, Auth } from '../interfaces';
import { ToDoData } from '../views/to_do/interfaces';

export interface AuthSelector extends DefaultReduxState<LoginPayload, Auth, string> { };
export function useAuthSelector() {
  const { action, data, err, fetch, res } = useSelector(({ auth }: ReduxAppState) => ({ ...auth })) as AuthSelector;

  return {
    authFetch: fetch,
    authData: data,
    authRes: res,
    authErr: err,
    authAction: action,
  };
};

export interface ToDoSelector extends DefaultReduxState<any, ToDoData[] | null> { };
export function useToDoSelector() {
  const { action, data, err, fetch, res } = useSelector(({ toDo }: ReduxAppState) => ({ ...toDo })) as ToDoSelector;

  return {
    fetchToDo: fetch,
    dataToDo: data,
    resToDo: res,
    errToDo: err,
    actionToDo: action,
  };
};

export interface AddToDoSelector extends DefaultReduxState<string | null> { };
export function useAddToDoSelector() {
  const { action, data, err, fetch, res } = useSelector(({ addToDo }: ReduxAppState) => ({ ...addToDo })) as AddToDoSelector;

  return {
    fetchAddToDo: fetch,
    dataAddToDo: data,
    resAddToDo: res,
    errAddToDo: err,
    actionAddToDo: action,
  };
};

export interface MarkAsDoneSelector extends DefaultReduxState<ToDoData | null, string | null> { };
export function useMarkAsDoneToDoSelector() {
  const { action, data, err, fetch, res } = useSelector(({ markAsDoneToDo }: ReduxAppState) => ({ ...markAsDoneToDo })) as MarkAsDoneSelector;

  return {
    fetchMarkAsDoneToDo: fetch,
    dataMarkAsDoneToDo: data,
    resMarkAsDoneToDo: res,
    errMarkAsDoneToDo: err,
    actionMarkAsDoneToDo: action,
  };
}

export function useSyncToDoSelector() {
  const { action, data, err, fetch, res } = useSelector(({ syncToDo }: ReduxAppState) => ({ ...syncToDo })) as DefaultReduxState;

  return {
    fetchSyncToDo: fetch,
    dataSyncToDo: data,
    resSyncToDo: res,
    errSyncToDo: err,
    actionSyncToDo: action,
  };
}

export function useNetworkSelector() {
  const { isOnline } = useSelector((state: ReduxAppState) => ({ isOnline: state.network.res }));

  return { isOnline };
};
