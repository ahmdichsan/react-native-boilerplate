import { DefaultReduxState } from './Redux';

export interface Auth {
  id: string;
  jwt: {
    token: string;
    expired: string;
  };
  name: string;
  userName: string;
};

export interface Profile {
  id: string;
  name: string;
  userName: string;
  createdBy: string;
  createdDate: string;
  lastUpdatedBy: string | null;
  lastUpdatedDate: string;
  activeFlag: boolean;
};

export interface LoginPayload {
  userName: string;
  password: string;
};
