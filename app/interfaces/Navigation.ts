import { NavigationInjectedProps, NavigationParams, NavigationScreenProp, NavigationRouteConfigMap, CreateNavigatorConfig, NavigationStackRouterConfig } from 'react-navigation';
import { StackNavigationOptions, StackNavigationProp, StackNavigationConfig } from 'react-navigation-stack/lib/typescript/src/vendor/types';

export declare interface NavigationProps extends NavigationInjectedProps<NavigationParams> { };

export declare interface CustomNavigation {
    navigation: NavigationScreenProp<NavigationParams>;
};

export declare interface NavigatiounRouter extends NavigationRouteConfigMap<StackNavigationOptions, StackNavigationProp> { };

export declare interface StackConfiguration extends CreateNavigatorConfig<
    StackNavigationConfig,
    NavigationStackRouterConfig,
    StackNavigationOptions,
    StackNavigationProp
> { };
