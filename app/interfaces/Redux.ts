/** @description: use this for default state redux */
export declare interface DefaultReduxState<Data = any, Res = any, Err = any> {
    action: string;
    fetch: boolean;
    data: Data | null;
    res: Res | null;
    err: Err | null;
};

/** @description use this for default action redux */
export declare interface DefaultReduxAction<Data = any, Res = any> {
    type: string;
    data?: Data;
    err?: string;
    res?: Res;
};

/** @description use this for default saga param */
export declare interface DefaultSagaParam {
    type: string;
};
