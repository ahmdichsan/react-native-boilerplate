import React, { useEffect } from 'react';
import { LogBox } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import { Container } from './app/bootstrap';
import { persistor, store } from './app/bootstrap/Bootstrap.stores';

function App() {
  useEffect(() => {
    LogBox.ignoreLogs([
      'Require cycle:'
    ]);

    SplashScreen.hide();
  }, []);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Container />
      </PersistGate>
    </Provider>
  );
};

export default App;
